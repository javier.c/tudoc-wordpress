<?php

include "kushki/autoload.php";


$merchantId = "10000002634048524915149912740811";
$idioma = KushkiLanguage::ES;
$moneda = KushkiCurrency::COP;
$entorno = KushkiEnvironment::TESTING;
 
$kushki = new Kushki($merchantId, $idioma, $moneda, $entorno);

//Comercios de Colombia
//print_r($_POST);
$token = $_POST["kushkiToken"];
//Comercios de Colombia
$planName = "Premium";
//Contacto detalles
$contactDetails = array(
            "firstName" => "Lisbeth",
            "lastName" => "Salander",
            "email" => "lisbeth@salander.com");
//El tiempo del plan
$periodicity = "monthly";
//LA fehca en la que comienza
$startDate = "2017-01-18";
$subtotalIva = 3200;
$iva = 608;
$subtotalIva0 = 0;
$propina = 10;
$tasaAeroportuaria = 0;
$agenciaDeViaje = 0;
$iac = 0;
$extraTaxes = new ExtraTaxes($propina, $tasaAeroportuaria, $agenciaDeViaje, $iac);
$monto = new Amount($subtotalIva, $iva, $subtotalIva0, $extraTaxes);
$metadata = array();

$subscription = $kushki->createSubscription($token, $planName, $periodicity, $contactDetails, $monto, $startDate, $metadata);


$msje = [];

if ($subscription->isSuccessful()) {
    $status = 0;
    $msje[] = "Número de ticket tt: " . $subscription->getSubscriptionId();
    //Businesses of Colombia
    $subscriptionId = $subscription->getSubscriptionId();
    

    $chargeSubscription = $kushki->chargeSubscription($subscriptionId, $metadata);

    if ($chargeSubscription->isSuccessful()) {
        $status = 1;
        $TN = $chargeSubscription->getTicketNumber();
        $msje[] = "Numero de ticket cs: ". $TN ;
        $metadata = array( 
            "id_paciente"=>$_POST["id_paciente"],
            "fecha_comienzo"=>$startDate,
            "subscriptionId"=>$subscriptionId,
            "ticketNumber"=>$TN,
            "tokenTarjeta"=>$token
            );
    } else {
        $status = 0;
        $msje[] = "Error " . $chargeSubscription->getResponseCode() . ": " . $chargeSubscription->getResponseText();
    }

} else {
    $status = 0;
    $msje[] = "Error " . $subscription->getResponseCode() . ": " . $subscription->getResponseText();
}
$metadata["msj"] = $msje;
header("Location: http://tudoc.dev/#/afiliacion?status=".$status."&res=".base64_encode(json_encode($metadata)) );
