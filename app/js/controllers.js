(function () {
  var paramsRegistro = {
    login: "",
    password: "",
    nombres: "",
    apellidos: "",
    Email: "",
    Nombre: "",
    full_name: "",
    Telefono: "",
    tag_list: "usuario",
    Sexo: "",
    Fecha_nacimiento: "",
    Peso: "",
    Altura: "",
    Enfermedades: [],
    Medicamentos: [],
    Alergias: [],
    Fuma: ""
  };
  angular
    .module("tudoc.controllers", [])
    .controller("mainController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        $scope.submitIniciar = function () {
          $window.location.href = "#/crear_usuario";
        };

        $scope.classBody = {
          fondo_land: true
        };

        function locationHashChanged() {
          console.log(location.hash);
          //setTimeout(function(){ iniciarSelect() }, 300);

          if ($window.location.hash == "#/categoria") {
            setTimeout(function () {
              iniciarSelect();
            }, 300);
          }
        }

        $window.onhashchange = locationHashChanged();
      }
    ])
    .controller("loginController", [
      "$scope",
      "$window",
      "tudocService",
      "facebookServices",
      function ($scope, $window, tudocService, facebookServices) {
        $scope.loginFacebook = function () {
          facebookServices
            .login()
            .then(function (user) {
              var formImg = {
                id_quickblox: user.id,
                id_fb: user.facebookData.id
              };
              console.log(formImg);

              facebookServices
                .saveImgFacebook(formImg)
                .then(function (data) {
                  console.dir(data);
                })
                .catch(function (err) {
                  console.trace(err);
                });

              $("#errorDiv").hide();
              $window.sessionStorage.setItem("user", JSON.stringify(user));
              //Como si existe lo ingreso a la plataforma
              if (user.user_tags) {
                if (user.user_tags.indexOf("Doctor") != -1) {
                  console.log("doctor");
                  $window.location.href = "#/chats";
                  //cargarChats()
                } else {
                  $window.location.href = "#/categoria";
                }
              } else {
                $window.location.href = "#/categoria";
              }
              //conecta al chat
              tudocService.chatConnect(user.id, user.facebookData.id);
              console.log(user);
            })
            .catch(function (err) {
              // console.log(err);
              var r_sexo = (err.data.gender = "male"
                ? "Masculino"
                : "Feminino");
              paramsRegistro.nombres = err.data.first_name;
              paramsRegistro.apellidos = err.data.last_name;
              paramsRegistro.Sexo = r_sexo;
              paramsRegistro.Email = err.data.email;
              paramsRegistro.login = err.data.email;
              paramsRegistro.password = err.data.id;
              $window.location.href = "#/crear_usuario";
              console.log(paramsRegistro);
            });
        };
        $scope.submitLogin = function () {
          var login = $("#login_usu").val();
          var password = $("#login_pass").val();

          var params = {
            login: login,
            password: password
          };

          $("#errorDiv").hide();
          tudocService
            .login(params)
            .then(function (user) {
              $("#errorDiv").hide();
              $window.sessionStorage.setItem("user", JSON.stringify(user));
              //Como si existe lo ingreso a la plataforma
              if (user.user_tags) {
                if (user.user_tags.indexOf("Doctor") != -1) {
                  console.log("doctor");
                  $window.location.href = "#/chats";
                  //cargarChats()
                } else {
                  $window.location.href = "#/categoria";
                }
              } else {
                $window.location.href = "#/categoria";
              }
              //conecta al chat
              tudocService.chatConnect(user.id, password);
            })
            .catch(function (err) {
              $(".msjalerta").text("Verfica tus credenciales");
              $(".cont-pop-up").show();
            });
          //No hago nada
          return false;
        };
      }
    ])
    .controller("recuperarController", [
      "$scope",
      "$window",
      "tudocService",
      "facebookServices",
      function ($scope, $window, tudocService, facebookServices) {

        $scope.cerrarAlert = function () {
          $window.location.href = "/"
        }

        $scope.submitRecover = function () {
          var login = $("#login_usu").val();

          var email = login;

          tudocService
            .resetPassword(email)
            .then(function (user) {
              console.log(user);
            })
            .catch(function (err) {
              $(".msjalerta").text("Hemos enviado un correo.");
              $(".cont-pop-up").show();
            });
          //No hago nada
          return false;
        };
      }
    ])
    .controller("pasosController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        $scope.dataF = paramsRegistro;

        var val_enfermedades = ($scope.reg_enfermedades = []);
        var val_medicamentos = ($scope.reg_medicamentos = []);
        var val_alergias = ($scope.reg_alergias = []);
        // tudocService.iniciarSelect();
        var vista = $scope;
        vista.step = "views/crear_paso1.html";

        $scope.submitPaso1Reg = function (e) {
          // Validar formulario
          // Paso 1
          var r_nombre = $("#reg_nombre").val();
          var r_apellido = $("#reg_apellido").val();
          var r_gender = $("input[name='gender_check']:checked").val();
          var r_email = $("#reg_email").val();
          var r_tel = $("#reg_tel").val();
          //Validar
          var r_pass = $("#reg_pass").val();
          var v_pass = $("#ver_pass").val();
          console.log(r_pass != v_pass);
          if (r_pass != v_pass) {
            $(".msjalerta").text("Las contraseñas no coinciden");
            $(".cont-pop-up").show();
            return false;
          }
          if (
            $("#dia-nacimiento").val() == null ||
            $("#mes-nacimiento").val() == null ||
            $("#anio-nacimiento").val() == null
          ) {
            $(".msjalerta").text("Completa fecha de nacimiento");
            $(".cont-pop-up").show();
            return false;
          }
          if (
            $("input[name='checkboxG1']:checked").length == 0 ||
            $("input[name='checkboxG2']:checked").length == 0
            /*||
               $("input[name='checkboxG3']:checked").length == 0 */
          ) {
            $(".msjalerta").text("Acepta los terminos y condiciones");
            $(".cont-pop-up").show();
            return false;
          }

          var r_birth =
            $("#dia-nacimiento").val() +
            "/" +
            $("#mes-nacimiento").val() +
            "/" +
            $("#anio-nacimiento").val();
          paramsRegistro.full_name = r_nombre + " " + r_apellido;
          paramsRegistro.Nombre = r_nombre + " " + r_apellido;
          paramsRegistro.Fecha_nacimiento = r_birth;
          paramsRegistro.Sexo = r_gender;
          paramsRegistro.Email = r_email;
          paramsRegistro.login = r_email;
          paramsRegistro.Telefono = r_tel;
          paramsRegistro.password = r_pass;
          //Redirect to step 2
          vista.step = "views/crear_paso4.html";
          //No hago nada
          return false;
        };
        $scope.submitPaso2Reg = function () {
          // Validar formulario
          // Paso 1
          //  var r_tel = $("#reg_tel").val();
          //  var r_email = $("#reg_email").val();
          // //set object sh*t
          //  paramsRegistro.phone  = r_tel
          //  paramsRegistro.email  = r_email
          //  paramsRegistro.login  = r_email
          //  //Redirect to step 2
          //  vista.step = 'views/crear_paso3.html';
          //  //No hago nada
          //  console.log(paramsRegistro)
          //No hago nada
          return false;
        };
        $scope.submitPaso3Reg = function () {
          // Validar formulario
          // Paso 1
          var r_enfermedad = $("input[name='enfermedad']").val();
          var r_medicamento = $("input[name='medicamento']").val();
          var r_alergico = $("input[name='alergico']").val();
          paramsRegistro.custom_data.preg1 = r_enfermedad;
          paramsRegistro.custom_data.preg2 = r_medicamento;
          paramsRegistro.custom_data.preg3 = r_alergico;
          //Redirect to step 2
          vista.step = "views/crear_paso4.html";
          //No hago nada
          console.log(paramsRegistro);
          //No hago nada
          return false;
        };
        $scope.submitPaso4Reg = function () {
          if (
            $("input[name='checkboxG3']:checked").length == 0 ||
            $("input[name='checkboxG4']:checked").length == 0
            /*||
               $("input[name='checkboxG3']:checked").length == 0 */
          ) {
            $(".msjalerta").text("Acepta los terminos y condiciones");
            $(".cont-pop-up").show();
            return false;
          }

          var r_peso = $("#reg_peso").val();
          var r_altura = $("#reg_altura").val();
          var r_enfermedades = JSON.stringify(val_enfermedades);
          var r_medicamentos = JSON.stringify(val_medicamentos);
          var r_alergias = JSON.stringify(val_alergias);
          var r_fuma = $("input[name='fuma']:checked").val();

          paramsRegistro.Peso = r_peso;
          paramsRegistro.Altura = r_altura;
          paramsRegistro.Fuma = r_fuma;
          paramsRegistro.Enfermedades = r_enfermedades;
          paramsRegistro.Medicamentos = r_medicamentos;
          paramsRegistro.Alergias = r_alergias;

          //Creo el user
          console.log("lala");
          console.log(paramsRegistro);
          $("#errorDivReg").hide();

          tudocService
            .createUser(paramsRegistro)
            .then(function (user) {
              $("#errorDivReg").hide();
              $window.sessionStorage.setItem("user", JSON.stringify(user));
              //Como si existe lo ingreso a la plataforma
              var params = {
                login: paramsRegistro.login,
                password: paramsRegistro.password
              };

              $window.open("#/login", "_self");
              $window.location.reload();
              $("#errorDiv").hide();
              // QB.login(params, function(err, user){
              //     if (user) {
              //             sessionStorage.setItem("user",JSON.stringify(user))
              //             window.location.href = "#/categoria"
              //           }
              // })
            })
            .catch(function (err) {
              console.log(err);
              $("#errorDivReg").show();
              if (err.detail.indexOf("password") != -1) {
                $(".msjalerta").text(
                  "La contrasena es muy corta min 8 caracteres."
                );
                $(".cont-pop-up").show();
                return;
              }
              if (err.detail.indexOf("login") != -1) {
                $(".msjalerta").text("Ya hay un usuario con esta cuenta");
                $(".cont-pop-up").show();
                return;
              }
              if (err.detail.indexOf("email") != -1) {
                $(".msjalerta").text("Ya hay un usuario con esta cuenta");
                $(".cont-pop-up").show();
                return;
              }
            });
          console.log("Guarda");
          //No hago nada
          return false;
        };
      }
    ])
    .controller("generalController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        console.log("Escoger General")
      }
    ])
    .controller("especialistaController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        console.log("Escoger Especialista");
      }
    ])
    .controller("categoriaController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {

        console.log("selec categoria")
        /**
         * BUSCAR UN MEDICO GENERAL
         */
        $scope.buscarGeneral() = function () {
          $window.location = "#/general"
        }
        /**
         * BUSCAR UN ESPECIALISTA 
         */
        $scope.buscarEspecialista() = function () {
          console.log("1")
          $window.location = "#/especialista"
        }
        // Validar formulario
        // Obtengo la categoria
        $scope.submitCategoria = function () {
          console.log("categoria");
          var categ = $("#select_categ").val();
          if (categ == null) {
            $(".msjalerta").text("Selecciona una categoria");
            $(".cont-pop-up").show();
            return false;
          }
          var params = {
            tags: categ
          };
          $("#loading_screen").show();
          $("#consulta_screen").hide();
          //asigna chat
          tudocService
            .asigChat(params)
            .then(function (user) {
              //Escojo un usuario aleatorio y creo un chat
              console.log("Creo un chat :)");

              console.log(user);
              var yo = JSON.parse(sessionStorage.getItem("user"));
              var doctor_id =
                user.items[Math.floor(Math.random() * user.items.length)].user
                  .id;
              console.warn(yo);
              console.warn(
                user.items[Math.floor(Math.random() * user.items.length)].user
              );
              //Redirijo al chat
              // var params = {
              //   type: 3,
              //   occupants_ids: [47]
              // };

              // QB.chat.dialog.create(params, function(err, createdDialog) {
              //   if (err) {
              //     console.log(err);
              //   } else {
              //   }
              // });

              var params = {
                type: 3,
                occupants_ids: [doctor_id],
                name: "Chat"
              };
              console.log(
                "Creating a dialog with params: " + JSON.stringify(params)
              );
              //crea sala de chat
              tudocService
                .createChat(params)
                .then(function (createdDialog) {
                  console.log(createdDialog);
                  $window.sessionStorage.setItem("DOCTOR_ID", doctor_id);
                  $window.location.href = "#/chat";
                  tudocService
                    .obtenerHistorial(createdDialog._id)
                    .then(function (messages) {
                      console.warn("DETALLE CHAT");
                      console.log(messages);
                      tudocService.pintarMsj(messages.items);

                      var docto = parseInt(sessionStorage.getItem("DOCTOR_ID"));
                      var params = {
                        filter: {
                          field: "id",
                          param: "in",
                          value: [docto]
                        }
                      };
                      //Aca le pinto el nav  y tda esa mondada
                      tudocService.verificarDoctor();
                      //user list
                      tudocService
                        .userList(params)
                        .then(function (result) {
                          console.warn("ESA ES LA INFOMACION DEL DOCOTR ");
                          console.log(result);
                          var doctor_info = result.items[0].user;
                          $(".nombre_doctor").text(
                            "Dr. " + doctor_info.full_name
                          );

                          if (doctor_info.custom_data) {
                            var items = doctor_info.custom_data.split(",");
                            $(".img_doctor").attr("src", items[0]);
                            $(".cargo_doctor").text(items[1]);
                          }
                        })
                        .catch(function (err) {
                          console.log(err);
                        });
                    })
                    .catch(function (err) {
                      console.log(err);
                    });
                })
                .catch(function (err) {
                  console.log(err);
                  alert("Session vencida");
                  $window.location.href = "#/";
                });
            })
            .catch(function (err) {
              $(".errorBuscar").text("No hemos encontrado medicos disponibles");
              $("#consulta_screen").show();
              console.log(err);
            });
          //No hago nada
          return false;
        };

      }
    ])
    .controller("chatsController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        tudocService
          .cargarChats()
          .then(function (resDialogs) {
            console.warn("lsisto  pues aca estan los chats actuales");
            console.log(resDialogs);

            //Pinto el listado de chats activos
            var cont_sin_leer = 0;
            for (var i = 0; i < resDialogs.items.length; i++) {
              if (resDialogs.items[i].unread_messages_count > 0) {
                cont_sin_leer++;
              }
            }
            $scope.chats = resDialogs.items;
            console.log($scope.chats);

            $(".sin_leer").text(cont_sin_leer);
          })
          .catch(function (err) {
            console.log(err);
          });

        $scope.chatDetail = function (chat_id, sender_id) {
          var chat_id = chat_id;
          var sender_id = sender_id;
          console.log(chat_id);
          console.log(sender_id);

          sessionStorage.setItem("DOCTOR_ID", sender_id);
          window.location.href = "#/chat";
          tudocService
            .obtenerHistorial(chat_id)
            .then(function (messages) {
              console.warn("DETALLE CHAT");
              console.log(messages);
              tudocService.pintarMsj(messages.items);

              var docto = parseInt(sessionStorage.getItem("DOCTOR_ID"));
              var params = {
                filter: {
                  field: "id",
                  param: "in",
                  value: [docto]
                }
              };
              //Aca le pinto el nav  y tda esa mondada
              tudocService.verificarDoctor();
              //user list
              tudocService
                .userList(params)
                .then(function (result) {
                  console.warn("ESA ES LA INFOMACION DEL DOCOTR ");
                  console.log(result);
                  var doctor_info = result.items[0].user;
                  $(".nombre_doctor").text("Dr. " + doctor_info.full_name);

                  if (doctor_info.custom_data) {
                    var items = doctor_info.custom_data.split(",");
                    $(".img_doctor").attr("src", items[0]);
                    $(".cargo_doctor").text(items[1]);
                  }
                })
                .catch(function (err) {
                  console.log(err);
                });
            })
            .catch(function (err) {
              console.log(err);
            });
        };
      }
    ])
    .controller("chatController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        console.log("chatDetail");
        var self = $scope;

        $scope.enviarMensajeBotonclick = function () {
          var currentText = $("#mensaje_chat").val().trim();

          if (!currentText.length) {
            return;
          }

          $("#mensaje_chat").val("").focus();

          tudocService.sendMessage(currentText, null);
          return false;
        };

        $scope.enviarImagenBotonclick = function () {
          $(".imagenload").click();
        };

        $(document).on("change", ".imagenload", function (e) {
          e.preventDefault();

          var inputFile = $(".imagenload")[0].files[0];

          // upload image
          tudocService
            .uploadImg(inputFile)
            .then(function (response) {
              console.log(response);

              console.log("LISTO PAPAP");
              var uploadedFile = response;
              console.log(uploadedFile);
              self.showImage(uploadedFile.uid, uploadedFile.name, false);
              tudocService.sendMessage("Adjunto", uploadedFile.uid);
            })
            .catch(function (err) {
              console.log(err);
            });
        });

        $(document).on("keypress", "#mensaje_chat", function (e) {
          if (e.which == 13) {
            self.enviarMensajeBotonclick();
          }
        });

        $scope.showImage = function (fileUID, fileName, toAppend) {
          console.warn("MUESTRO FOTO :)")
          var img_thumb =
            '<div class="contenedor_mensaje ">' +
            '<div class="caja_mensaje_izq ">' +
            '<img src="' + tudocService.privateUrl(fileUID) + '" alt="" class="imagen_detalle_chat">' +
            '<p class="hora_mensaje ">07:40</p>' +
            '</div>' +
            '<div class="separador_izq "></div>' +
            '</div>';

          $(".mensajes").append(img_thumb);
        };
      }
    ])
    .controller("perfilController", [
      "$scope",
      "$window",
      "tudocService",
      function ($scope, $window, tudocService) {
        var user = JSON.parse($window.sessionStorage.getItem("user"));
        var self = $scope;
        var idUser = user.id;

        tudocService
          .infoUsuario(idUser)
          .then(function (data) {
            console.log(data);
            self.dataUsuario = data.items[0];
            var fbd = self.dataUsuario.Fecha_nacimiento;
            fbd = fbd.split("/");
            var fbdDay = fbd[0];
            var fbdMont = fbd[1];
            var fbdYear = fbd[2];

            self.reg_enfermedades = JSON.parse(self.dataUsuario.Enfermedades);
            self.reg_medicamentos = JSON.parse(self.dataUsuario.Medicamentos);
            self.reg_alergias = JSON.parse(self.dataUsuario.Alergias);

            self.dataUsuario.fbdDay = fbdDay;
            self.dataUsuario.fbdMont = fbdMont;
            self.dataUsuario.fbdYear = fbdYear;
            console.log(data);
          })
          .catch(function (err) {
            console.error(err);
          });

        $(".select_bottom").click(function () {
          $(".filefield").trigger("click");
        });
        $(".filefield").change(function () {
          if ($(this).val() != "") {
            $(".overlay_uploader").show();
            $(".spinner").show();
            readURL2(this);
          }
        });
        function readURL2(input) {
          if (
            input.files[0].type == "image/jpeg" ||
            input.files[0].type == "image/png"
          ) {
            $.each(jQuery(input)[0].files, function (i, file) {
              var reader = new FileReader();
              reader.onload = function (e) {
                $(".overlay_uploader").hide();
                $(".spinner").hide();
                $(".box").css(
                  "background-image",
                  "url(" + e.target.result + ")"
                );
              };
              reader.readAsDataURL(input.files[0]);
            });
          } else {
            $(".overlay_uploader").hide();
            $(".spinner").hide();
            alert("Solo se permiten archivos .jpg y .png");
          }
        }
        var paramsUpdate = {};
        $scope.submitUpdatePerfil = function () {
          var r_nombre = $("#reg_nombre").val();
          var r_gender = $("input[name='gender_check']:checked").val();
          var r_email = $("#reg_email").val();
          var r_tel = $("#reg_tel").val();
          //Validar
          var r_pass = $("#reg_pass").val();
          var v_pass = $("#ver_pass").val();
          var o_pass = $("#old_pass").val();

          if (r_pass != v_pass) {
            $(".msjalerta").text("Las contraseñas no coinciden");
            $(".cont-pop-up").show();
            return false;
          }
          if (
            $("#dia-nacimiento").val() == null ||
            $("#mes-nacimiento").val() == null ||
            $("#anio-nacimiento").val() == null
          ) {
            $(".msjalerta").text("Completa fecha de nacimiento");
            $(".cont-pop-up").show();
            return false;
          }
          var b_pass = true;
          var r_birth =
            $("#dia-nacimiento").val() +
            "/" +
            $("#mes-nacimiento").val() +
            "/" +
            $("#anio-nacimiento").val();
          if (
            o_pass.length != null &&
            r_pass.length != null &&
            v_pass.length != null &&
            r_pass === v_pass
          ) {
            var data = {
              password: r_pass,
              old_password: o_pass
            };
            var userId = self.dataUsuario.user_id;
            tudocService
              .updatePassword(userId, data)
              .then(function (result) {
                $("#reg_pass").val("");
                $("#ver_pass").val("");
                $("#old_pass").val("");
              })
              .catch(function (err) {
                console.log("error");
                console.error(err);
                $("#errorDivReg").show();
                if (err.detail.indexOf("old") != -1) {
                  b_pass = false;
                  $(".msjalerta").text(
                    "La contraseña antigua no coincide, verifica (Si te registraste por facebook no puedes cambiar tu contraseña)"
                  );
                  $(".cont-pop-up").show();
                  return false;
                }
                if (err.detail.indexOf("password") != -1) {
                  b_pass = false;
                  $(".msjalerta").text(
                    "La contrasena es muy corta min 8 caracteres."
                  );
                  $(".cont-pop-up").show();
                  return false;
                }
                if (err.detail.indexOf("login") != -1) {
                  b_pass = false;
                  $(".msjalerta").text("Ya hay un usuario con esta cuenta");
                  $(".cont-pop-up").show();
                  return false;
                }
                if (err.detail.indexOf("email") != -1) {
                  b_pass = false;
                  $(".msjalerta").text("Ya hay un usuario con esta cuenta");
                  $(".cont-pop-up").show();
                  return false;
                }
              });
          }
          paramsUpdate._id = self.dataUsuario._id;
          paramsUpdate.Nombre = r_nombre;
          paramsUpdate.Fecha_nacimiento = r_birth;
          paramsUpdate.Sexo = r_gender;
          paramsUpdate.Email = r_email;
          paramsUpdate.login = r_email;
          paramsUpdate.Telefono = r_tel;

          tudocService
            .updateInfo(paramsUpdate)
            .then(function (result) {
              if (b_pass) {
                $(".msjalerta").text("Se actualizaron los datos!");
                $(".cont-pop-up").show();
              }
            })
            .catch(function (err) {
              $(".msjalerta").text(
                "Algo salio mal, intentalo denuevo mas tarde!"
              );
              $(".cont-pop-up").show();
              console.error(err);
            });
        };
        $scope.submitUpdateHClinica = function () {
          var r_peso = $("#reg_peso").val();
          var r_altura = $("#reg_altura").val();
          var r_enfermedades = JSON.stringify(self.reg_enfermedades);
          var r_medicamentos = JSON.stringify(self.reg_medicamentos);
          var r_alergias = JSON.stringify(self.reg_alergias);
          var r_fuma = $("input[name='fuma']:checked").val();

          paramsUpdate._id = self.dataUsuario._id;
          paramsUpdate.Peso = r_peso;
          paramsUpdate.Altura = r_altura;
          paramsUpdate.Fuma = r_fuma;
          paramsUpdate.Enfermedades = r_enfermedades;
          paramsUpdate.Medicamentos = r_medicamentos;
          paramsUpdate.Alergias = r_alergias;

          tudocService
            .updateInfo(paramsUpdate)
            .then(function (result) {
              $(".msjalerta").text("Se actualizaron los datos!");
              $(".cont-pop-up").show();
            })
            .catch(function (err) {
              $(".msjalerta").text(
                "Algo salio mal, intentalo denuevo mas tarde!"
              );
              $(".cont-pop-up").show();
              console.error(err);
            });
        };
      }
    ]);
})();
