<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Historias;

class HistoriasController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json');

header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

        $Historias = Historias::all();
        //Aca se van a aplicar los filtros a todos los request 
        //print_r($_GET);
        echo $Historias->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //print_r($request);
        header('Content-type: application/json');

        header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

        $Historia = new Historias;

        $Historia->CHAT_ID = $request->CHAT_ID;
        $Historia->COSTO = $request->COSTO;
        $Historia->DESCRIPCION = $request->DESCRIPCION;
        $Historia->MEDICO_FOTO = $request->MEDICO_FOTO;
        $Historia->MEDICO_ID = $request->MEDICO_ID;
        $Historia->MEDICO_NOMBRE = $request->MEDICO_NOMBRE;
        $Historia->PACIENTE_ID = $request->PACIENTE_ID;
        $Historia->PACIENTE_NOMBRE = $request->PACIENTE_NOMBRE;
        $Historia->PENDIENTE = $request->PENDIENTE;
        $Historia->PUNTUACION = $request->PUNTUACION;
        
        $id =  $Historia->save();

        $Historias = Historias::find($id);

        echo $Historias->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
