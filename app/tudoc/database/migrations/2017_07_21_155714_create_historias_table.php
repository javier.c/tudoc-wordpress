<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historias', function (Blueprint $table) {
            $table->increments('id');
            /* Integer */
            $table->integer('CHAT_ID');
            /* Float */
            $table->float('COSTO', 8, 2);
            /* String */
            $table->string('DESCRIPCION');
            /* String */
            $table->string('MEDICO_FOTO');
            /* Integer */
            $table->integer('MEDICO_ID');
            /* String */
            $table->string('MEDICO_NOMBRE');
            /* Integer */
            $table->integer('PACIENTE_ID');
            /* String */
            $table->string('PACIENTE_NOMBRE');
            /* Boolean */
            $table->boolean('PENDIENTE');
            /* Integer */
            $table->integer('PUNTUACION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historias');
    }
}
