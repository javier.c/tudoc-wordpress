<?php 
/*template name: Quienes_Somos*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css">

<div class="container-wrap">
	
	
	<div class="container main-content">
		
		<div class="row">

	
			<?php // if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php // the_content(); ?>
	
			<?php // endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


	<div class="sass_container">
		<div class="container-fluid container-principal">
			<div class="row quienesSomos">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb70">
					<h2 class="titulo__1 ta-c">
						¿QUIÉNES SOMOS?
					</h2>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding quienesSomos__quien">
					<div class="quienesSomos__quien--box">
						<div>
							<p class="texto__1 texto__morado">
								<strong>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</strong>
							</p>
							<p class="texto__1 mt20">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
							</p>
						</div>
					</div>
					<div class="quienesSomos__quien--img">
						<img src="<?php bloginfo('template_url'); ?>/deploy/img//quienes_somos/quienesSomos.jpg" alt="">
					</div>
				</div>
			</div>

			<div class="separador"></div>
			<div class="row ta-c mt50 mb50">
				<a href="/app" class="boton boton__horizontal">SOLICITA TU CONSULTA</a>
			</div>
			<div class="separador"></div>

			<div class="row nuestroServicio">
				<div class="container">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<h2 class="titulo__1">
							NUESTRO SERVICIO
						</h2>
						<p class="texto__2 mt30">
							Explicamos la norma para que todos logren apropiarla y aplicarla, Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperi am, eaque ipsa quae.
						</p>
					</div>
					<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
						<img src="<?php bloginfo('template_url'); ?>/deploy/img//quienes_somos/imagen-quienes-somos.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<?php get_footer(); ?>