<?php 
/*template name: Blog*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css"> -->

<div class="container-wrap">


	<!-- <div class="sass_container">
		<div class="container-fluid container-principal">
			<div class="row">
				
			</div>
		</div>
	</div> -->
	
	
	<div class="container main-content otroDeEstosPaddings">
		
		<div class="row sass_container">

			<h2 class="titulo__1" style="
				color: #917df3;
				font-size: 45px;
				font-weight: 800;
				line-height: 50px;
				margin-bottom: 70px;
				text-align: center;">
				Blog
			</h2>
					
	
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>
	
			<?php endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


</div>


<style>
	.otroDeEstosPaddings{
		padding-top: 180px;
	}
	@media only screen and (max-width: 1000px) and (min-width: 1px){
		.otroDeEstosPaddings{
			padding-top: 0;
		}
	}
</style>

<?php get_footer(); ?>