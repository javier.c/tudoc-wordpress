<?php 
/*template name: Dudas*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css"> -->

<div class="container-wrap">


	<!-- <div class="sass_container">
		<div class="container-fluid container-principal">
			<div class="row">
				
			</div>
		</div>
	</div> -->
	
	
	<div class="container main-content cont-dudas">
		<div class="row">

	
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>
	
			<?php endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


</div>


<?php get_footer(); ?>