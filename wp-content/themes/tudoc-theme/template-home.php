<?php 
/*template name: Home*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css">
<script src="https://unpkg.com/flickity@2.0.9/dist/flickity.pkgd.min.js"></script>

<div class="container-wrap">
	
	
	<div class="container main-content">
		
		<div class="row">

	
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>
	
			<?php endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


	<div class="sass_container">
		<div class="container-fluid container-principal">
			<div class="row homeConsultados">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb40">
					<h2 class="titulo__1 ta-c">
						TEMAS MÁS CONSULTADOS
					</h2>
				</div>
				<!-- ↓↓↓↓ Carrusel Comentado ↓↓↓↓ -->
				<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding">
					<div class="carousel" data-flickity='{ "contain": true }'>
						<div class="carousel-cell">
							<img src="<?php bloginfo('template_url'); ?>/deploy/img/home/consultados_1.jpg" alt="">
							<h3>PEDIATRÍA</h3>
						</div>
						<div class="carousel-cell">
							<img src="<?php bloginfo('template_url'); ?>/deploy/img/home/consultados_2.jpg" alt="">
							<h3>GINECOLOGÍA</h3>
						</div>
						<div class="carousel-cell">
							<img src="<?php bloginfo('template_url'); ?>/deploy/img/home/consultados_3.jpg" alt="">
							<h3>GINECOLOGÍA</h3>
						</div>
						<div class="carousel-cell">
							<img src="<?php bloginfo('template_url'); ?>/deploy/img/home/consultados_4.jpg" alt="">
							<h3>GINECOLOGÍA</h3>
						</div>
					</div>
				</div> -->
			</div>

			<!-- <div class="separador"></div>
			<div class="row ta-c mt50 mb50">
				<a href="" class="boton boton__horizontal">SOLICITA TU CONSULTA</a>
			</div>
			<div class="separador"></div> -->

			<div class="row homeAfiliacion">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ta-c no_padding">
					<h2 class="titulo__1">
						AFILIACIÓN
					</h2>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 homeAfiliacion__info box_shadow">
					<p class="texto__1 mt30">
						Explicamos la norma para que todos logren apropiarla y aplicarla, Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperi am, eaque ipsa quae.
					</p>
					<p class="titulo__2 mt20">
						$12.500
					</p>
					<a href="/app" class="boton boton__horizontal boton__horizontal--pequeno mt30">COMPRAR</a>
				</div>
			</div>
		</div>
	</div>

</div>


<?php get_footer(); ?>