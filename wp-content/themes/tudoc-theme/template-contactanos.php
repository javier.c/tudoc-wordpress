<?php 
/*template name: Contactanos*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css">

<div class="container-wrap">
	
	
	<div class="container main-content">
		
		<div class="row">

	
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>
	
			<?php endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


	<div class="sass_container">
		<div class="container container-principal">
			<div class="row contactanos">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ta-c mb40">
					<h2 class="titulo__1 mt70">
						CONTÁCTANOS
					</h2>
					<p class="texto__2">
						Déjanos tus datos personales y te contactaremos. Antes de entregarnos tus datos consulta nuestras 
					</p>
					<p class="texto__2">
						<strong>
							Políticas Tratamiento de la Información y la Autorización para el tratamiento de la información.
						</strong>
					</p>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="box_shadow">
						<h3>
							<strong>Conoce más sobre nosotros</strong>
						</h3>
						<p class="texto__2 mt10">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</p>
						<?php
							echo do_shortcode(
								'[contact-form-7 id="1650" title="Contact form 1"]'
							);
						?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<p class="texto__terminos">
						Aviso de Privacidad. <br>
						En cumplimiento de la Ley 1581 de 2012 y el Decreto Reglamentario 1377 de 2013, le informamos que los datos personales que usted nos ha entregado serán almacenados, usados, circulados, actualizados y en general tratados con la siguiente finalidad: (i) Creación de usuarios y otorgamiento de accesos para la prestación de los servicios de capacitación por medios virtuales., (ii) Envío de información relacionada con el objeto social de TUDOC (iii) Contacto por medio telefónico o correo electrónico para ofrecer la prestación de servicios. (iv) Desarrollo de estudios de mercado enfocados en la actividad de tudoc., (v) Facturación., (vi) Transmitir los datos personales a servidores ubicados en los Estados Unidos de Norteamérica, Canadá, a países latinoamericanos y europeos y cualquier otro país para los fines del tratamiento autorizado o para propósitos de almacenamiento y/o eventuales servicios de hosting o cloud computing (computación en la nube) que tudoc contrate o requiera. (vii) En caso de utilizar la opción de pago PayU, PayU recolectará información de forma directa por medio de su plataforma, autorizándolo a procesar los pagos, validar las transacciones, enviar información promocional y publicitaria, de los comercios registrados en la Plataforma PayU y de terceros que incluyan promociones para la compra de bienes o servicios a través de la Plataforma PayU, enviar cupones de descuento, afiliar a programas de lealtad, crear perfiles de consumo del pagador, entre otros materiales promocionales y publicitarios que puedan llegar a ser de interés del pagador, de conformidad con las políticas establecidas por PayU y publicadas en su página web. Página web: www.Ttudoc.com, con correo electrónico info@inteligam.com. El manejo de dichos datos se hará de acuerdo a lo establecido en las Políticas de Tratamiento de la Información de tu doce publicadas en la página web www.inteligam.com/politicas-tratamiento-de-la-informacion/ que contiene las políticas establecidas por tu doc para el tratamiento de los datos, los mecanismos y procedimientos para la efectividad de los derechos del Titular de la información a: (i) Conocer, actualizar y rectificar mis Datos Personales.
					</p>
				</div>
			</div>

		</div>
	</div>

</div>


<?php get_footer(); ?>