<?php 
/*template name: Detalle Blog*/
get_header(); ?>


<?php nectar_page_header($post->ID);  ?>

<?php 
$options = get_nectar_theme_options();
wp_enqueue_script('nectarMap', get_template_directory_uri() . '/js/map.js', array('jquery'), '1.0', TRUE);
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/deploy/css/main.css">

<div class="container-wrap">
	
	
	<div class="container main-content">
		
		<div class="row">

	
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>
	
			<?php endwhile; endif; ?>

		</div>
		
	</div><!--/container-->


	<div class="sass_container">
		<div class="container-fluid container-principal">

			<!-- <div class="separador"></div>
			<div class="row ta-c mt50 mb50">
				<a href="" class="boton boton__horizontal">SOLICITA TU CONSULTA</a>
			</div>
			<div class="separador"></div> -->

			<div class="row pageAfiliacion">
				<div class="container-fluid">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ta-c no_padding">
						<h2 class="titulo__1">
							Afiliación
						</h2>
					</div>
				</div>
				<div class="container box_shadow">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pageAfiliacion__left">
						<div>
							<p class="texto__1 texto__morado">
								<strong>Sed ut perspiciatis unde </strong>
							</p>
							<p class="texto__1 mt20">
								<strong>omnis</strong> iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi.
							</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pageAfiliacion__right">
						<div class="pageAfiliacion__right--precio box_shadow">
							<div>
								<img src="<?php bloginfo('template_url'); ?>/deploy/img/afiliacion/afiliacion.png" alt="">
								<p class="titulo__2 mt20">
									$70.000
								</p>
								<p>
									<a href="" class="boton boton__horizontal boton__horizontal--pequeno mt30">COMPRAR</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<?php get_footer(); ?>