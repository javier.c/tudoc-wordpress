<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mass_tudoc');

/** MySQL database username */
define('DB_USER', 'mass_docdev');

/** MySQL database password */
define('DB_PASSWORD', 'gDNx5Nxsmw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=2z%|W4U?zH{Y%Me/>l6 _$JSF!Vg$82HS[X^R^+@^Xcv]G[rJBn9y+I:F/o:)In');
define('SECURE_AUTH_KEY',  '0ghAJ#`6G?3/KeP)u5X)u=LR@n2qPsS)5WF:tb+>e7}ldT}o(Pp+BIa@4>7~mZ9-');
define('LOGGED_IN_KEY',    'w4gGNXjEM8W8NJIXo7DTF5GrABj*cDk*_$PP=6t@)19%qRX?@xzQ5sV<`Aygkjo]');
define('NONCE_KEY',        'B)QwniFl63Dm^M7OBLT,D1ZE5_TJm+CspQ1p}IGa[-a?.6$1-;-{B]zs6>LD9^p?');
define('AUTH_SALT',        'yyM)M4@Q%|;fPoyb`%%,Pt<z8{bKE$4r%=HM]|Q+t9W7n0<qQs6{ef`RrU~M}OX ');
define('SECURE_AUTH_SALT', 'ruu$9R&>LHK+.m<T>!5P()UYSJi#2;-,lT2d+OY:;?I2iWBB:C?kXbfr+EASRQ/@');
define('LOGGED_IN_SALT',   'B}P&!E.ZQx2zdCRN{F${)zfI|}s+SF<sPHmmSh/SGn@^mUH*0qxdzW`0C?<DLgUW');
define('NONCE_SALT',       '1aFg}:C+{Uzw1B:xcfcmO$,~_W1jno#C?1=_T&t]oqzzk6?Bxlo/?qPFIuLJJc!2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'td_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
